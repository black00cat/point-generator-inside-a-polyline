<?php

namespace Polyline;
use JetBrains\PhpStorm\Pure;

/**
 * It is used to generate a polygon using an encoded polyline with this tool:
 * https://developers.google.com/maps/documentation/utilities/polylineutility
 * Useful for selecting an area, then generating points systematically inside the area,
 * in order to optimise the search
 */
class Polyline
{
    /**
     * Distance at which the points are being generated
     */
    const DEFAULT_POINTS_DENSITY = 0.15;

    const EARTH_RADIUS_IN_KM = 6371.01;

    private array $polylineLatitudes;

    private array $polylineLongitudes;

    private int $numberOfVertices = 0;

    /**
     * @throws \Exception
     */
    public function __construct(string $encodedPolyline)
    {
        $decodedPolyline = $this->decodePolyline($encodedPolyline);

        if (empty($decodedPolyline)) {
            throw new \Exception('Polyline cannot be decoded');
        }

        foreach ($decodedPolyline as $point) {
            if (isset($point[1])) {
                $this->polylineLatitudes[] = $point[0];
                $this->polylineLongitudes[] = $point[1];
                $this->numberOfVertices++;
            }
        }
    }

    /**
     * Method that decodes the polyline encoded inside the tool
     * https://developers.google.com/maps/documentation/utilities/polylineutility
     */
    private function decodePolyline(string $encodedPolyline): array
    {
        //Step 11) unpack the string as unsigned char 'C'
        $bytes = array_merge(unpack('C*', $encodedPolyline));
        $results = [];

        $index = 0; //tracks which char in $byte
        do {
            $shift = 0;
            $result = 0;
            do {
                if (!isset($bytes[$index])) {
                    return $results;
                }

                //Step 10
                $char = $bytes[$index] - 63;

                // Steps 9-5
                // get the least significant 5 bits from the byte
                // and bitwise-or it into the result
                $result |= ($char & 0x1F) << (5 * $shift);
                $shift++;
                $index++;
            } while ($char >= 0x20); // Step 8 most significant bit in each six bit chunk
            // is set to 1 if there is a chunk after it and zero if it's the last one
            // so if char is less than 0x20 (0b100000), then it is the last chunk in that num

            // Step 3-5 - sign will be stored in least significant bit, if it's one, then
            // the original value was negated per step 5, so negate again
            if ($result & 1) {
                $result = ~$result;
            }

            // Step 4-1) shift off the sign bit by right-shifting and multiply by 1E-5
            $result = ($result >> 1) * 0.00001;
            $results[] = $result;
        } while ($index < count($bytes));

        // to save space, lat/lons are deltas from the one that preceded them, so we need to
        // adjust all the lat/lon pairs after the first pair
        for ($i = 2; $i < count($results); $i++) {
            $results[$i] += $results[$i - 2];
        }

        // chunk the array into pairs of lat/lon values
        return array_chunk($results, 2);
    }

    /**
     * Method that generates points inside the polygon bordered by the given polyline.
     */
    public function generatePointsInsidePolylineByDistance($distanceInKm): array
    {
        $bearing = 90;
        $originalBearing = $bearing;

        // constructs a rectangle that borders the polyline polygon
        $latN = max($this->polylineLatitudes);
        $latS = min($this->polylineLatitudes);
        $lonW = min($this->polylineLongitudes);
        $lonE = max($this->polylineLongitudes);

        // we have to get the longest distance between E and W points of 2D rectangle (one may be longer than the other)
        $distanceBetweenEW = max(
            $this->distanceInKmBetweenEarthCoordinates($latN, $lonW, $latN, $lonE),
            $this->distanceInKmBetweenEarthCoordinates($latS, $lonW, $latS, $lonE)
        );
        $distanceBetweenSN = $this->distanceInKmBetweenEarthCoordinates($latS, $lonW, $latN, $lonW);

        $distanceBetweenPoints = $distanceInKm / self::EARTH_RADIUS_IN_KM;

        $generatedPoints = array();
        $numberOfGeneratedPoints = 0;
        $countY = 0;

        $newPoint = array($latS, $lonW);
        $generatedPoints[] = $newPoint;

        for ($y = 0; $y <= $distanceBetweenSN; $y += $distanceInKm) {
            if ($y !== 0) {
                $bearing = 360;
                $newPoint = $this->calculateNewPointCoordinatesFromPreviousPoint($newPoint, $bearing, $distanceBetweenPoints);
            }

            for ($x = 0; $x <= $distanceBetweenEW; $x += $distanceInKm) {
                $bearing = ($countY % 2 === 0) ? $originalBearing : (360 - $originalBearing);
                $newPoint = $this->calculateNewPointCoordinatesFromPreviousPoint($newPoint, $bearing, $distanceBetweenPoints);

                if ($this->isPointInPolygon($newPoint)) {
                    $generatedPoints[] = $newPoint;
                    $numberOfGeneratedPoints++;
                }

                if ($x + $distanceInKm > $distanceBetweenEW && $distanceBetweenEW - $x < $distanceInKm) {
                    $x = $distanceBetweenEW;
                }
            }

            if ($y + $distanceInKm > $distanceBetweenSN && $distanceBetweenSN - $y < $distanceInKm) {
                $y = $distanceBetweenSN;
            }

            $countY++;
        }

        print_r("Generated $numberOfGeneratedPoints points inside the polygon at a distance of $distanceInKm km between each other\n");
        return $generatedPoints;
    }

    #[Pure] private function calculateNewPointCoordinatesFromPreviousPoint(array $newPoint, float $bearing, float $distanceBetweenPoints): array
    {
        $lat = $this->degreesToRadians($newPoint[0]);
        $long = $this->degreesToRadians($newPoint[1]);
        $bearing = $this->degreesToRadians($bearing);

        $lat2 = asin(sin($lat) * cos($distanceBetweenPoints) + cos($lat) * sin($distanceBetweenPoints) * cos($bearing));
        $lon2 = $long + atan2(sin($bearing) * sin($distanceBetweenPoints) * cos($lat), cos($distanceBetweenPoints) - sin($lat) * sin($lat2));
        $lon2 = fmod(($lon2 + 3 * pi()), (2 * pi())) - pi();

        return [$this->radiansToDegrees($lat2), $this->radiansToDegrees($lon2)];
    }

    /**
     * Function that generates points inside the polygon bordered by the given polyline.
     * The points are generated at a given density.
     */
    public function generatePointsInsidePolylineByDensity($pointsDensity = self::DEFAULT_POINTS_DENSITY): array
    {
        // constructs a rectangle that borders the polyline polygon
        $latN = max($this->polylineLatitudes);
        $latS = min($this->polylineLatitudes);
        $lonW = min($this->polylineLongitudes);
        $lonE = max($this->polylineLongitudes);

        $generatedPoints = array();
        $numberOfGeneratedPoints = 0;

        // goes through all coordinates of density x in the rectangle,
        for ($x = $latS; $x <= $latN; $x += $pointsDensity) {
            for ($y = $lonW; $y <= $lonE; $y += $pointsDensity) {
                $newPoint = array($x, $y);
                // filtering the point if it's not inside the polygon
                if ($this->isPointInPolygon($newPoint)) {
                    $generatedPoints[] = $newPoint;
                    $numberOfGeneratedPoints++;
                }
                if ($y + $pointsDensity > $lonE && $lonE - $y < $pointsDensity) {
                    $y = $lonE;
                }
            }
            if ($x + $pointsDensity > $latN && $latN - $x < $pointsDensity) {
                $x = $latN;
            }
        }

        print_r("Generated $numberOfGeneratedPoints points inside polygon :)\n");
        return $generatedPoints;
    }

    /**
     * Function used to calculate the distance between two coordinates.
     * Distance is computed using the Haversine formula.
     */
    #[Pure] private function distanceInKmBetweenEarthCoordinates(float $lat1, float $lon1, float $lat2, float $lon2): float
    {
        $dLat = $this->degreesToRadians($lat2 - $lat1);
        $dLon = $this->degreesToRadians($lon2 - $lon1);

        $lat1 = $this->degreesToRadians($lat1);
        $lat2 = $this->degreesToRadians($lat2);

        $a = sin($dLat / 2) * sin($dLat / 2) +
            sin($dLon / 2) * sin($dLon / 2) * cos($lat1) * cos($lat2);
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));

        return self::EARTH_RADIUS_IN_KM * $c;
    }

    private function degreesToRadians(float $degrees): float
    {
        return $degrees * pi() / 180;
    }

    private function radiansToDegrees(float $radians): float
    {
        return $radians * 180 / pi();
    }

    /**
     * Function that checks if a certain point of coordinates (lat, lng) is inside the
     * polygon bordered by the given polyline.
     * Each side of the polygon is compared to the longitude coordinate of the test point. Afterwards, it determines
     * a list of nodes, where each node is a point where one side crosses the longitude threshold of the test point
     * Function returns true if there is an odd number of nodes on each side of the test point, and false if there
     * is an even number of nodes on each side of the test point.
     */
    public function isPointInPolygon(array $newPoint): bool
    {
        $numberOfPolylineCorners = $this->numberOfVertices;

        $j = $numberOfPolylineCorners - 1;
        $hasOddNodes = false;

        for ($i = 0; $i < $numberOfPolylineCorners; $i++) {
            if (($this->polylineLongitudes[$i] < $newPoint[1] && $this->polylineLongitudes[$j] >= $newPoint[1] ||
                    $this->polylineLongitudes[$j] < $newPoint[1] && $this->polylineLongitudes[$i] >= $newPoint[1]) &&
                ($this->polylineLatitudes[$i] + ($newPoint[1] - $this->polylineLongitudes[$i]) /
                    ($this->polylineLongitudes[$j] - $this->polylineLongitudes[$i]) * ($this->polylineLatitudes[$j] - $this->polylineLatitudes[$i]) < $newPoint[0])) {
                $hasOddNodes = !$hasOddNodes;
            }

            $j = $i;
        }

        return $hasOddNodes;
    }
}