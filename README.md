# Point generator inside a Polyline

Description
This tool is used to generate a polygon with an encoded polyline that is constructed using this tool: 

https://developers.google.com/maps/documentation/utilities/polylineutility

or a full screen one:

https://google-developers.appspot.com/maps/documentation/utilities/polyline-utility/polylineutility

It is useful for selecting a country's borders, then generating points systematically only inside the border, in order to optimise the number of requests made to search inside an area (usually with a compexity of O(n^2) ).

 

Declared variables
const
DEFAULT_POINTS_DENSITY
0.15
Distance at which the points are being generated
private
$arrLat
 	
Array of polyline vertices latitudes
private
$arrLon
 	
Array of polyline vertices longitudes
private
$intVertices
 	
Number of vertices
 

Functions
public
__construct
$strEncodedPolyline
The Polyline constructor. It accepts an encoded polyline as parameter, it decodes it, then, from the resulted array of points, it constructs the arrays of longitudes and latitudes of those points and calculates the number of vertices of the polygon.

Throws an exception in case the Polyline encoded string can't be decoded.

private
_decodePolyline
$strEncodedPolyline
Function that decodes the polyline that has been encoded in the polylineutility tool. The logic is the same as here, but reverse engineered:

https://developers.google.com/maps/documentation/utilities/polylinealgorithm?csw=1

public
_generatePointsInsidePolylineByDensity
$fltPointsDensity = 
DEFAULT_POINTS_DENSITY
 
Function that generates points inside the polygon bordered by the given polyline. It accepts an custom optional density, if needed.

It constructs a rectangle that borders the polyline polygon using the smallest and largest latitude and longitude. Afterwards, it generates points systematically inside that rectangle, and, while going through all the coordinates inside the rectangle, it filters the ones that are not inside the polygon using the function_isPointInPolygon.


public
_generatePointsInsidePolylineByDistance
$intDistanceInKm
This function is basically the same as the _generatePointsInsidePolylineByDensity, but a bit more complex, since it generates them at a given distance in kilometers.

It stars the same, by constructing the rectangle that borders the polyline polygon using the smallest and respectively largest latitude and longitude. Afterwards, we have to simply compute the distance between these extremities using the _distanceInKmBetweenEarthCoordinates function. The trick is that this is a bit more 3Dish than the other point-generating function, because this rectangle isn't really a rectangle in 2D. This is why we have to get the longest distance between E and W points of the constructed 2D rectangle, because one side may be longer than the other (i.e. if the rectangle is above the equator line, the bottom part will be longer).



In this algorithm, there is another difference, that is the points are generated differently, due to the bearing that is needed to calculate the next point at a certain distance in kilometers. To calculate the next pint, the latitude, longitude and bearing is transformed into radians, in order to apply the formula. For each point generated, its location is again transformed into degrees and checked if it is inside the shape, just like in the previous algorithm.

private
_toRad
$fltDeg
Function that transforms radians into degrees.
private
_toDeg
$fltRad
Function that transforms degrees into radians.
private
_getLargestCoord
$arrCoords
Function that returns the largest coordinate in an array of coordinates.
private
_getSmallestCoord
$arrCoords
Function that returns the smallest coordinate in an array of coordinates.
public
_isPointInPolygon
$arrNewPoint
Function that checks if a certain point of coordinates (lat, lng) is inside the polygon bordered by the given polyline.

Each side of the polygon is compared to the longitude coordinate of the test point. Afterwards, it determines a list of nodes, where each node is a point where one side crosses the longitude threshold of the test point.

The function returns true if there is an odd number of nodes on each side of the test point, and false if there is an even number of nodes on each side of the test point.

(Tip: Check the Ray Casting Algorithm)

 

Example
We make a polyline for USA that also includes a part of Alaska and Hawaii, such that the points generated should be minimum. Notice the small path that was between the mainland USA and Hawaii, respectively Alaska. In here, theoretically there won't be generated any points if it is small enough.

 



The points are generated inside the polyline at a density of 0.8. This is how they look.



 

(To generate a map of your own, you only need to generate a CSV that has the latitudes and longitudes of all points. Afterwards, you go to My Maps and create a new map. You add a new layer to the map and import your CSV in it).

Almost good, right? But we have to make sure that the points generated are close to the border as possible, and in some cases, as you can see, the border with Mexico is a bit unclear, so we need to reposition the points in the polyline, then generate again the map.



Perfect! (smile)